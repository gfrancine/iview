import { el } from "./elbuilder";
import { FsModule } from "./fs";
import { AppMenu, AppOptions, FS } from "./types";
import { Viewer } from "./viewer";
import { Navigator } from "./navigator";
import { Shortcuts } from "./shortcuts";
import { Emitter } from "./emitter";

export class App {
  element: HTMLElement;
  fs: FS;
  appmenu: AppMenu = new Emitter();
  viewer: Viewer;
  options: AppOptions;
  shortcuts: Shortcuts = new Shortcuts();
  navigator: Navigator;

  constructor(opts: AppOptions) {
    this.options = opts;

    if (opts.appmenu) this.appmenu = opts.appmenu;

    this.element = el("div")
      .class("app-container")
      .parent(opts.element)
      .element;

    this.fs = new FsModule(opts.fs);
    this.viewer = new Viewer(this);
    this.navigator = new Navigator(this);
  }

  async init() {
    await this.navigator.init();
  }
}
