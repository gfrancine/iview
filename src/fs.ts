import { FS } from "./types";
import { posix } from "./path-browser";

export class FsModule implements FS {
  fs?: Partial<FS>;

  constructor(fs?: Partial<FS>) {
    this.fs = fs;
  }

  fileExists(path: string) {
    if (this.fs?.fileExists) return this.fs.fileExists(path);
    return Promise.resolve(true);
  }

  resolveImageSrc(path: string) {
    if (this.fs?.resolveImageSrc) return this.fs.resolveImageSrc(path);
    return path;
  }

  getDir(path: string) {
    if (this.fs?.getDir) return this.fs.getDir(path);
    return posix.parse(path).dir;
  }

  getExtension(path: string) {
    if (this.fs?.getExtension) return this.fs.getExtension(path);
    const withTheDot = posix.parse(path).ext;
    if (withTheDot.length < 1) return "";
    return withTheDot.slice(1);
  }

  recursiveReadDir(path: string) {
    if (this.fs?.recursiveReadDir) return this.fs.recursiveReadDir(path);
    return Promise.resolve([]);
  }

  absolute(path: string) {
    if (this.fs?.absolute) return this.fs.absolute(path);
    return path;
  }

  dialogOpenFile(filters: { name: string; extensions: string[] }[]) {
    if (this.fs?.dialogOpenFile) return this.fs.dialogOpenFile(filters);
    return Promise.resolve();
  }
}
