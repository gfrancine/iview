import { Emitter } from "./emitter";

export type AppOptions = {
  element: Element;
  initialFile?: string;
  fs?: Partial<FS>;
  appmenu?: AppMenu;
};

export type FsEntry = {
  path: string;
  children?: FsEntry[];
};

export type FsDialogFilter = {
  name: string;
  extensions: string[];
};

export type FS = {
  fileExists: (path: string) => Promise<boolean>;
  resolveImageSrc: (path: string) => string;
  recursiveReadDir: (path: string) => Promise<FsEntry[]>;
  getDir: (path: string) => string;
  getExtension: (path: string) => string;
  absolute: (path: string) => string;
  dialogOpenFile: (filters: FsDialogFilter[]) => Promise<string | void>;
};

export type AppMenu = Emitter<{
  next: () => unknown;
  previous: () => unknown;
  open: () => unknown;
}>;
