// file navigator

import { App } from "./app";
import { FsDialogFilter, FsEntry } from "./types";

function makeRecord(values: string[]) {
  const record: Record<string, true> = {};
  for (const v of values) {
    record[v] = true;
  }
  return record;
}

const IMAGE_EXTS_FILTERS: FsDialogFilter[] = [
  {
    name: "PNG",
    extensions: ["png"],
  },
  {
    name: "JPG/JPEG",
    extensions: ["jpg", "jpeg"],
  },
  {
    name: "SVG",
    extensions: ["svg"],
  },
];

const IMAGE_EXTS_LIST: string[] = [];

for (const filter of IMAGE_EXTS_FILTERS) {
  for (const extension of filter.extensions) {
    IMAGE_EXTS_LIST.push(extension);
  }
}

const IMAGE_EXTS_MAP = makeRecord(IMAGE_EXTS_LIST);

export class Navigator {
  private app: App;
  private currentDir?: string;
  private nextFiles: string[] = [];
  private currentFile?: {
    path: string;
    index: number;
  };

  constructor(app: App) {
    this.app = app;
  }

  async init() {
    const initialFile = this.app.options.initialFile;
    if (initialFile) {
      const path = this.app.fs.absolute(initialFile);
      if (this.isImageFile(path) && await this.app.fs.fileExists(path)) {
        await this.openFile(initialFile);
      }
    }

    this.app.shortcuts.register(["ArrowLeft"], this.previous);
    this.app.shortcuts.register(["ArrowRight"], this.next);
    this.app.shortcuts.register(["Control", "O"], this.dialogOpenFile);
    this.app.shortcuts.register(["Command", "O"], this.dialogOpenFile);

    this.app.appmenu.on("next", this.next);
    this.app.appmenu.on("previous", this.previous);
    this.app.appmenu.on("open", this.dialogOpenFile);
  }

  private isImageFile(path: string) {
    const ext = this.app.fs.getExtension(path);
    return IMAGE_EXTS_MAP[ext.toLowerCase()] === true;
  }

  private async openFile(path: string) {
    const dir = this.app.fs.getDir(path);
    await this.refreshNextFiles(dir);
    this.currentDir = dir;
    this.setCurrentFileByPath(path);
  }

  private dialogOpenFile = async () => {
    const path = await this.app.fs.dialogOpenFile(IMAGE_EXTS_FILTERS);
    if (!path) return;
    this.openFile(path);
  };

  private next = () => this.move(true);
  private previous = () => this.move(false);

  private move(next: boolean) {
    if (!this.currentFile) return;
    if (this.nextFiles.length < 1) return;
    const current = this.currentFile;
    const nextIndex = next
      ? current.index === this.nextFiles.length - 1 ? 0 : current.index + 1
      : current.index === 0
      ? this.nextFiles.length - 1
      : current.index - 1;

    this.setCurrentFileByIndex(nextIndex);
  }

  private async refreshNextFiles(dir: string) {
    const tree = await this.app.fs.recursiveReadDir(dir);
    const finalPaths: string[] = [];

    const recurse = (entries: FsEntry[]) => {
      for (const entry of entries) {
        if (entry.children) {
          recurse(entry.children);
        } else {
          finalPaths.push(entry.path);
        }
      }
    };

    recurse(tree);
    this.nextFiles = finalPaths;
  }

  private reset() {
    this.setCurrentFile();
    this.nextFiles = [];
    delete this.currentDir;
  }

  private setCurrentFileByIndex(index: number) {
    const path = this.nextFiles[index];
    if (!path) return;
    const file = { path, index };
    this.setCurrentFile(file);
    this.handleFilePossiblyNotExisting(file);
  }

  private async handleFilePossiblyNotExisting(
    file: { path: string; index: number },
  ) {
    // if someone deleted the next file, try refreshing the
    // directory and continuing from the previous index
    if (!await this.app.fs.fileExists(file.path)) {
      if (this.currentDir) {
        let canReadDirectory = true;

        try {
          await this.refreshNextFiles(this.currentDir);
        } catch {
          // directory might've been deleted
          canReadDirectory = false;
        }

        if (canReadDirectory) {
          const pathAtIndex = this.nextFiles[file.index];
          if (pathAtIndex) {
            this.setCurrentFile({
              path: pathAtIndex,
              index: file.index,
            });
          } else if (this.nextFiles.length > 0) {
            const index = this.nextFiles.length - 1;
            this.setCurrentFile({
              path: this.nextFiles[index],
              index: index,
            });
          } else {
            this.setCurrentFile();
          }
        } else {
          this.reset();
        }
      } else {
        this.reset();
      }
    }
  }

  private setCurrentFileByPath(path: string) {
    let index = this.nextFiles.indexOf(path);
    if (index === -1) {
      index = 0;
      this.nextFiles.unshift(path);
    }

    this.setCurrentFile({ path, index });
  }

  private setCurrentFile(currentFile?: { path: string; index: number }) {
    this.currentFile = currentFile;
    if (currentFile) this.app.viewer.view(currentFile.path);
  }
}
