import { App } from "./app";
import { FsWebModule } from "./fs-web";
import { el } from "./elbuilder";
import { AppMenu } from "./types";
import { Emitter } from "./emitter";

import "../styles/app.scss";
import "iv-viewer/dist/iv-viewer.css";

(async () => {
  const root = document.querySelector("#root") as HTMLElement;

  const example =
    "https://www.google.com/images/branding/googlelogo/2x/googlelogo_dark_color_272x92dp.png";

  const appmenu: AppMenu = new Emitter();

  el("div")
    .children(
      el("button")
        .inner("Open File")
        .on("click", () => appmenu.emit("open"))
        .element,
      el("button")
        .inner("Next")
        .on("click", () => appmenu.emit("next"))
        .element,
      el("button")
        .inner("Previous")
        .on("click", () => appmenu.emit("previous"))
        .element,
    )
    .parent(root)
    .element;

  const app = new App({
    element: root,
    fs: new FsWebModule(),
    initialFile: example,
    appmenu,
  });

  await app.init();

  ((window as unknown) as { app: App }).app = app;
})();
