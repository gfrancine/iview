// https://github.com/s-yadav/iv-viewer

type Options = {
  zoomValue?: number;
  maxZoom?: number;
  snapView?: boolean;
  refreshOnResize?: boolean;
  zoomOnMouseWheel?: boolean;
  hasZoomButtons?: boolean;
  zoomSteps?: number;
  // listeners
};

declare class ImageViewer {
  constructor(container: Element, options?: Options);
  load(...urls: string[]);
}

declare module "iv-viewer" {
  export default ImageViewer;
}
