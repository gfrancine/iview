export class Shortcuts {
  private pressedKeys: Record<string, true> = {};

  constructor() {
    window.addEventListener("focus", () => {
      this.pressedKeys = {};
    });

    window.addEventListener("keyup", (e) => {
      delete this.pressedKeys[e.key];
    });

    window.addEventListener("keydown", (e) => {
      this.pressedKeys[e.key] = true;
    });
  }

  register(keys: string[], callback: () => unknown) {
    const keydownListener = () => {
      for (const key of keys) {
        if (!this.pressedKeys[key]) return;
      }
      return callback();
    };

    window.addEventListener("keydown", keydownListener);

    return () => {
      window.removeEventListener("keydown", keydownListener);
    };
  }
}
