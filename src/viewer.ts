import { App } from "./app";
import ImageViewer from "iv-viewer";
import { el } from "./elbuilder";

export class Viewer {
  private app: App;
  private element: HTMLElement;
  private viewer: ImageViewer;

  constructor(app: App) {
    this.app = app;

    this.element = el("div")
      .class("viewer-container")
      .parent(app.element)
      .element;

    this.viewer = new ImageViewer(this.element, {
      snapView: false,
    });
  }

  view(path: string) {
    const src = this.app.fs.resolveImageSrc(path);
    this.viewer.load(src);
  }
}
